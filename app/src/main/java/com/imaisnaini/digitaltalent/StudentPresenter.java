package com.imaisnaini.digitaltalent;

import com.imaisnaini.digitaltalent.db.Student;
import com.imaisnaini.digitaltalent.db.StudentDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentPresenter {
    public StudentPresenter() {
    }

    public void loadData(){
        Student student1 = new Student("Fatimah Isnaini", "imaisnaini@gmail.com", "imaisnaini");
        Student student2 = new Student("Nadilla Choirul", "nadilla@gmail.com", "nadilla");
        Student student3 = new Student("Novarita Milianti", "novarita@gmail.com", "novarita");

        try {
            StudentDao.getStudentDao().add(student1);
            StudentDao.getStudentDao().add(student2);
            StudentDao.getStudentDao().add(student3);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Student getByID(Integer id){
        Student student = null;
        try {
            student = StudentDao.getStudentDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }

    public List<Student> loginStudent(String email, String password){
        List<Student> studentList = new ArrayList<>();
        try {
            studentList = StudentDao.getStudentDao().getStudent(email, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public List<Student> getAllStudent(){
        List<Student> studentList = new ArrayList<>();
        try {
            studentList = StudentDao.getStudentDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentList;
    }

    public List<String> getListNama(){
        List<Student> studentList = new ArrayList<>();
        List<String> namaList = new ArrayList<>();
        try {
            studentList = StudentDao.getStudentDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < studentList.size(); i++){
            namaList.add(studentList.get(studentList.size()-i-1).getName());
        }
        return namaList;
    }

    public void addStudent(String nama, String email, String password){
        Student student = new Student(nama, email, password);

        try {
            StudentDao.getStudentDao().add(student);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
