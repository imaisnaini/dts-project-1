package com.imaisnaini.digitaltalent.db;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.query.In;

import java.sql.SQLException;
import java.util.List;

public class StudentDao extends BaseDaoCrud<Student, Integer> {
    private static  StudentDao studentDao;

    public static StudentDao getStudentDao(){
        if (studentDao == null){
            studentDao = new StudentDao();
        }
        return studentDao;
    }

    public List<Student> getStudent(String email, String password) throws SQLException {
        QueryBuilder<Student, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Student.EMAIL, email).and().eq(Student.PASSWORD, password);
        return getDao().query(qb.prepare());
    }

    public Student getByID(Integer id) throws SQLException{
        return getDao().queryForId(id);
    }
}
