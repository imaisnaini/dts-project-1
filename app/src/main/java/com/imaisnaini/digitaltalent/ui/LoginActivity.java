package com.imaisnaini.digitaltalent.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.digitaltalent.utils.DialogBuilder;
import com.imaisnaini.digitaltalent.R;
import com.imaisnaini.digitaltalent.StudentPresenter;
import com.imaisnaini.digitaltalent.db.Student;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.imaisnaini.digitaltalent.utils.Utils.isEempty;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.tietEmail_activityLogin)
    TextInputEditText etLogin;
    @BindView(R.id.tietPassword_activityLogin)
    TextInputEditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    private boolean validate(){
        if (isEempty(etLogin)){
            etLogin.setError(getString(R.string.error_field));
            return false;
        }
        if (isEempty(etPassword)){
            etPassword.setError(getString(R.string.error_field));
            return false;
        }
        return true;
    }

    private void doLogin(String email, String password){
        List<Student> student = new StudentPresenter().loginStudent(email, password);

        if (student.isEmpty()){
            DialogBuilder.showErrorDialog(LoginActivity.this, "Email atau Password salah!");
        }else{
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("id", student.get(0).getId());
            startActivity(intent);
            finish();
        }
    }

    @OnClick(R.id.btnLogin_activityLogin) public void onLogin(){
        if (validate()) {
            doLogin(etLogin.getText().toString(), etPassword.getText().toString());
        }
    }

    @OnClick(R.id.tvRegister_activityLogin) public void goRegister(){
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }
}
