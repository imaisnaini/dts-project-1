package com.imaisnaini.digitaltalent.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.digitaltalent.R;
import com.imaisnaini.digitaltalent.StudentPresenter;
import com.imaisnaini.digitaltalent.db.Student;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.imaisnaini.digitaltalent.utils.Utils.isEempty;

public class MainActivity extends AppCompatActivity {
    private StudentPresenter presenter;
    private Student activeStudent;
    private Integer idStudent;
    private List<String> studentList = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.tietEmail_activityMain)
    TextInputEditText etEmail;
    @BindView(R.id.tietPassword_activityMain)
    TextInputEditText etPassword;
    @BindView(R.id.tietNama_activityMain)
    TextInputEditText etNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initData();
        initViews();
    }

    private void initData(){
        presenter = new StudentPresenter();
        idStudent = getIntent().getIntExtra("id", 0);
        activeStudent = presenter.getByID(idStudent);
        studentList = presenter.getListNama();
    }

    private void initViews(){
        tvUsername.setText("Hello " + activeStudent.getName());

        adapter = new ArrayAdapter<>(MainActivity.this, R.layout.item_student, R.id.tvNama, studentList);
        listView.setAdapter(adapter);
    }

    private void reload(){
        adapter.clear();
        adapter = new ArrayAdapter<>(MainActivity.this, R.layout.item_student, R.id.tvNama, presenter.getListNama());
        listView.setAdapter(adapter);
    }

    private boolean validate(){
        if (isEempty(etEmail)){
            etEmail.setError(getString(R.string.error_field));
            return false;
        }
        if (isEempty(etPassword)){
            etPassword.setError(getString(R.string.error_field));
            return false;
        }
        if (isEempty(etNama)){
            etEmail.setError(getString(R.string.error_field));
            return false;
        }
        return true;
    }

    @OnClick(R.id.btnAddStudent) void addStudent(){
        if (validate()){
            presenter.addStudent(etNama.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString());
            reload();
            etEmail.setText("");
            etPassword.setText("");
            etNama.setText("");
        }
    }
}
