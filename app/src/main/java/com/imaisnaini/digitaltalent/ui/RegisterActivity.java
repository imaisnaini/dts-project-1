package com.imaisnaini.digitaltalent.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.imaisnaini.digitaltalent.R;
import com.imaisnaini.digitaltalent.StudentPresenter;
import com.imaisnaini.digitaltalent.utils.DialogBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.imaisnaini.digitaltalent.utils.Utils.isEempty;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.tietEmail_activityRegister) TextInputEditText etEmail;
    @BindView(R.id.tietPassword_activityRegister) TextInputEditText etPassword;
    @BindView(R.id.tietPhone_activityRegister) TextInputEditText etPhone;
    @BindView(R.id.tietBirthdate_activityRegister) TextInputEditText etBirthDate;
    @BindView(R.id.tietRePassword_activityRegister) TextInputEditText etRePassword;
    @BindView(R.id.rgGender_activityRegister) RadioGroup rgGender;
    @BindView(R.id.tietName_activityRegister) TextInputEditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private boolean validate(){
        if (isEempty(etEmail)){
            etEmail.setError(getString(R.string.error_field));
            return false;
        }
        if (isEempty(etPassword)){
            etPassword.setError(getString(R.string.error_field));
            return false;
        }
        if (isEempty(etName)){
            etEmail.setError(getString(R.string.error_field));
            return false;
        }
        if (isEempty(etRePassword)){
            etPassword.setError(getString(R.string.error_field));
            return false;
        }
        return true;
    }

    @OnClick(R.id.btnRegister_activityRegister) void onRegister(){
        if (validate()){
            String email = etEmail.getText().toString();
            String password = etPassword.getText().toString();
            String nama = etName.getText().toString();

            new StudentPresenter().addStudent(nama, email, password);
            DialogBuilder.showNotifDialog(RegisterActivity.this, "Register", "Pendaftaran berhasil. Silahkan Login untuk melanjutkan.", new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                    finish();
                }
            });
        }
    }
}
