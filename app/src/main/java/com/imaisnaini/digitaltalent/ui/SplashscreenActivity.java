package com.imaisnaini.digitaltalent.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.imaisnaini.digitaltalent.R;
import com.imaisnaini.digitaltalent.StudentPresenter;
import com.imaisnaini.digitaltalent.db.Db;
import com.imaisnaini.digitaltalent.utils.PreferenceManager;

public class SplashscreenActivity extends AppCompatActivity {

    public static final int STORAGE_PERMISSION = 20;
    private PreferenceManager mPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        Db.getInstance().init(this);
        mPrefManager = new PreferenceManager(this);
        if (mPrefManager.isFirstTimeLaunch()) {
            new StudentPresenter().loadData();
            mPrefManager.setFirstTimeLaunch(false);
        }

        //ask permission to read storage
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashscreenActivity.this, LoginActivity.class));
                finish();
            }
        }, 2000);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Recieved.", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this, "Permission Denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
