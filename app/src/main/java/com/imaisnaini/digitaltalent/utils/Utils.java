package com.imaisnaini.digitaltalent.utils;

import android.widget.EditText;

import static android.text.TextUtils.isEmpty;

public class Utils {
    public static boolean isEempty(EditText text){
        CharSequence str = text.getText().toString();
        return isEmpty(str);
    }
}
